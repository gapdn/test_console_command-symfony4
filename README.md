**Symfony4 console project**
---
**[Задание](#task)**

**[Установка](#install)**

**[MySQL](#mysql)**

---

#Task

*PHP*

Существует несколько видов работников: программист, дизайнер, тестировщик, менеджер.

Каждый работник умеет по-своему делать свою работу:

- программист может: 1) писать код, 2) тестировать код, 3) общаться с менеджером
- дизайнер может: 4) рисовать, 3) общаться с менеджером
- тестировщик может: 2) тестировать код, 3) общаться с менеджером, 5) ставить задачи - менеджер может: 5) ставить задачи

В свою очередь они не умеют:

- программист: 4) рисовать 5) ставить задачи
- дизайнер: 1) писать код 2) тестировать код 5) ставить задачи - тестировщик: 1) писать код 4) рисовать
- менеджер: 1) писать код 4) рисовать 2) тестировать код
задание:
- нужно описать умение каждого сотрудника с помощь принципов ООП;
- написать консольную команду средствами Symfony (Symfony console commands). В качестве параметра команда должна принимать название должности. Результатом роботы команды должен бить список уменей работника. Для написания логики используйте сервисы.
пример запуска:
php bin/console company:employee programmer
на выходе должны получить подобное: - code writing
- code testing
- communication with manager
- также реализовать может ли сотрудник делать определенные действия. Пример для реализации:
команда:
php ./vendor/bin/console employee:can programmer writeCode
результат: true
команда:
php ./vendor/bin/console employee:can programmer draw
результат: false
Требования
1. php 7 и выше
2. Symfony 4
2. Использовать код стайл psr-2 https://www.php-fig.org/psr/psr-2/
3. Создать новый репозиторий на https://bitbucket.org/ и залить туда
 
*MySQL*

1. Для заданного списка товаров получить названия всех категорий, в которых представлены товары.
Выборка для нескольких товаров (пример: ids = (9, 14, 6, 7, 2) ).

2. Для заданной категории получить список предложений всех товаров из этой категории. Каждая категория может иметь несколько подкатегорий.

Пример:
Выбираю все товары из категории компьютеры (id = 2) и подкатегории (id =3 (ноутбуки), id = 4 (планшеты), id = 5 (гибриды) ).

3. Для заданного списка категорий получить количество уникальных товаров в каждой категории.
Выборка для нескольких категорий (пример: ids = (2, 3, 4) ).

4. Для заданного списка категорий получить количество единиц каждого товара который входит в указанные категории.
Выборка для нескольких категорий (пример: ids = (3, 4, 5) ).

Примечание:
Схема БД создается самостоятельно на основе выше представленых требований. В результате вы должны предоставить схему БД и SQL запросы.

---

#Install

Для установки приложени достаточно клонировать репозиторий выполнив команду:
```ruby
git clone https://gapdn@bitbucket.org/gapdn/keyua-console_command.git
```
**Команды используемые в приложение:**

```
company:employee [<position>] //получение умений работника
```

Пример запроса:
```php
php bin/console company:employee programmer

//ответ
code writing
code testing
communication with manager
```
---
```php
php bin/console employee:can [<position>] [<ability>] //проверка наличия умений 
```

Пример запроса:
```php
php bin/console employee:can programmer writeCode

//ответ
true
```

Принимаемые аргументы:

[position]

**programmer**

**tester**

**designer**

**manager**

[ability]

**writeCode**

**testCode**

**communication**

**setTask**

**draw**

---

#Mysql

**Схема БД**

```mysql
CREATE DATABASE `key` CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `categories` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`parent_category_id` int(11) unsigned NOT NULL DEFAULT 0,
	`path` varchar(255) DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX (`parent_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_to_category` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`product_id` int(11) NOT NULL,
	`category_id` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `product_to_category_product_id_category_id_idx` (`product_id`, `category_id`)
) ENGINE=InnoDB;
```

**Тестовые данные**

```mysql
INSERT INTO categories (name, parent_category_id, path) VALUES ('computer', 0, '1'), ('notebook', 1, '1.2'), ('hybrid', 1, '1.3'), ('tablet', 1, '1.4'), ('phone', 0, '5'), ('smartphone', 5, '5.6'), ('oldphone', 5, '5.7'), ('pocketphone', 5, '5.8'), ('tv', 0, '9'), ('instrument', 0, '10');
INSERT INTO products (name) VALUES ('macBook PRO 15'), ('macBook PRO 13'), ('macBook AIR'), ('iPad'), ('Lenovo'), ('DLL'), ('Computer'), ('iPhone SE'), ('iPhone 7'), ('iPhone 8'), ('generalelectric'), ('Simens'), ('SonyEricson'), ('LenovoHybrid'), ('YamahaPiano'), ('Gibson'), ('Ibanez'), ('ESP'), ('Crafter');
INSERT INTO product_to_category (product_id, category_id) VALUES (1, 2), (2, 2), (3, 2), (4, 4), (5, 2), (6, 2), (7, 1), (8, 6), (9, 6), (10, 6), (11, 5), (12, 7), (13, 8), (14, 3), (15, 10), (16, 10), (17, 10), (18, 10), (19, 10);
```

**Запросы**

```mysql
1. SELECT c.name FROM product_to_category LEFT JOIN categories AS c ON (category_id = c.id) WHERE product_id IN (1, 19, 5) GROUP BY c.name;
2. SELECT category_id, p.name FROM product_to_category LEFT JOIN products AS p ON (product_id = p.id) LEFT JOIN categories as c ON (category_id = c.id) WHERE c.path LIKE '5%';
3. SELECT category_id, COUNT(p.name) FROM product_to_category LEFT JOIN products AS p ON (product_id = p.id) WHERE category_id IN (10, 6, 2) GROUP BY category_id;
4. SELECT category_id, p.amount, p.name FROM product_to_category LEFT JOIN products AS p ON (product_id = p.id) WHERE category_id IN (10, 6, 2);
```

