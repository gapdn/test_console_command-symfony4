<?php

namespace App\Entity;

use App\Entity\Programmer;
use App\Entity\Designer;
use App\Entity\Tester;
use App\Entity\Manager;

class EmployeeDepartment 
{
    private static $programmer = 'programmer';
    private static $tester = 'tester';
    private static $designer = 'designer';
    private static $manager = 'manager';  
    
    static public function getWorker($position) 
    {
        switch ($position) {
            case self::$programmer:
                return new Programmer();
                break;
            case self::$designer:
                return new Designer();
            case self::$tester:
                return new Tester();
                break;
            case self::$manager:
                return new Manager();
                break;
            default:
                return false;
                break;
        }
        
        return false;
    }
    
    static private function getListOfPositions() 
    {
        return [
            self::$programmer,
            self::$tester,
            self::$designer,
            self::$manager,
        ];
    }
    
    static public function getStringOfPositions() 
    {
        return implode(PHP_EOL, self::getListOfPositions());
    }
}
