<?php

namespace App\Entity;

class Designer extends Employee 
{
    public function __construct() 
    {
        $this->position = 'designer';
    }
}
