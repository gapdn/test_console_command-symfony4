<?php

namespace App\Entity;

use App\Components\Abilities;

abstract class Employee 
{
    protected $position = '';
    
    public function can($ability) 
    {
        return Abilities::can($this, $ability);
    }
    
    public function getStringOfAbilities() 
    {
        return Abilities::getAbilitiesInString($this);
    }
}
