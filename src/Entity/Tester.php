<?php

namespace App\Entity;

class Tester extends Employee 
{
    public function __construct() 
    {
        $this->position = 'tester';
    }
}
