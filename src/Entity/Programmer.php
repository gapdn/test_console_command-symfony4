<?php

namespace App\Entity;

class Programmer extends Employee 
{
    public function __construct() 
    {
        $this->position = 'programmer';
    }    
}
