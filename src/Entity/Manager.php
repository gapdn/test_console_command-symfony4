<?php

namespace App\Entity;

class Manager extends Employee 
{
    public function __construct() 
    {
        $this->position = 'manager';
    }
}
