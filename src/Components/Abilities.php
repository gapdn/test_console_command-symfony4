<?php

namespace App\Components;

use App\Entity\Employee;

class Abilities 
{
    const ABILITIES = [
        'writeCode' => 'code writing',
        'testCode' => 'code testing', 
        'communication' => 'communication with manager',
        'setTask' => 'set tasks', 
        'draw' => 'draw',
    ];
    
    public static function can(Employee $worker, $ability) 
    {
        if (!array_key_exists($ability, self::ABILITIES)) {
            return false;
        }
        
        if (!array_key_exists($ability, self::getAbilitiesInArray($worker))) {
            return false;
        }
        
        return true;
    }
    
    private static function getAbilitiesInArray(Employee $worker)
    {
        if ($worker instanceof \App\Entity\Programmer) {
            return [
                'writeCode' => 'code writing',
                'testCode' => 'code testing',
                'communication' => 'communication with manager',
            ];
        } elseif ($worker instanceof \App\Entity\Tester) {
            return [
                'setTask' => 'set tasks', 
                'testCode' => 'code testing',
                'communication' => 'communication with manager',
            ];
        } elseif ($worker instanceof \App\Entity\Designer) {
            return [
                'draw' => 'draw',
                'communication' => 'communication with manager',
            ];
        } elseif ($worker instanceof \App\Entity\Manager) {
            return [
                'setTask' => 'set tasks',
            ];
        } else {
            return [];
        }
    }
    
    public static function getAbilitiesInString(Employee $worker)
    {        
        return implode(PHP_EOL, self::getAbilitiesInArray($worker));
    }
    
    public static function getAllAbilitiesInString() 
    {
        return implode(PHP_EOL, array_keys(self::ABILITIES));
    }
}
