<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Components\Abilities;
use App\Entity\EmployeeDepartment;

class CandoCommand extends Command 
{
    protected function configure()
    {
        $help = 'Available positions:' . PHP_EOL . EmployeeDepartment::getStringOfPositions() . PHP_EOL
                .'Available abilities:' . PHP_EOL . Abilities::getAllAbilitiesInString();
                
        $this->setName('employee:can')
            ->setDescription("Prints can an employee has the ability")
            ->addArgument('position', InputArgument::OPTIONAL, 'Pass the position.')
            ->addArgument('ability', InputArgument::OPTIONAL, 'Pass the ability.')
            ->setHelp($help)
            ->ignoreValidationErrors();
    }
 
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getResultString(
                $input->getArgument('position'),
                $input->getArgument('ability')
            )
        );
    }
    
    private function getResultString($position, $ability) 
    {
        $worker = EmployeeDepartment::getWorker($position);
        
        if (empty($worker)) {
            return 'Wrong arguments. Use --help comand to find out the truth.';
        }
        
        return $worker->can($ability) ? 'true' : 'false';
    }
}
