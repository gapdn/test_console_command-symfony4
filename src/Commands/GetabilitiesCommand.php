<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Entity\EmployeeDepartment;

class GetabilitiesCommand extends Command 
{
    protected function configure()
    {
        $help = 'Available positions:' . PHP_EOL . EmployeeDepartment::getStringOfPositions();
        $this->setName('company:employee')
            ->setDescription("Prints employee's abilities")
            ->addArgument('position', InputArgument::OPTIONAL, 'Pass the position.')
            ->setHelp($help)
            ->ignoreValidationErrors();
    }
 
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getResultString($input->getArgument('position')));
    }
    
    private function getResultString($position = '') 
    {
        $worker = EmployeeDepartment::getWorker($position);
        
        if (empty($worker)) {
            return 'Wrong argument. Use --help comand to find out the truth.';
        }
        
        return $worker->getStringOfAbilities();
    }
}
